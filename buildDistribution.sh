#!/bin/bash

set -e

ACCEPTED_JAVA_VERSIONS=(11 17)

NAME=smartgears-distribution
SMARTGEARS_VERSION=$(mvn org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=project.version -q -DforceStdout)
echo $SMARTGEARS_VERSION
JAVA_VERSION=17

mvn dependency:get -Dartifact=org.gcube.core:common-smartgears:${SMARTGEARS_VERSION} -Dtransitive=false
TOMCAT_VERSION=$(unzip -p ~/.m2/repository/org/gcube/core/common-smartgears/${SMARTGEARS_VERSION}/common-smartgears-${SMARTGEARS_VERSION}.jar \
        META-INF/maven/org.gcube.core/common-smartgears/pom.xml \
        | grep '<tomcat.version>' \
        | sed -E 's/.*<tomcat.version>(.*)<\/tomcat.version>.*/\1/')
echo "TOMCAT_VERSION: ${TOMCAT_VERSION}"


PUSH_DOCKER=false
PUSH_HARBOR=false
LOGIN_HARBOR=false
HARBOR_REPO=hub.dev.d4science.org
MULTI_PLATFORM=false


################################################################################
# Help                                                                         #
################################################################################
Help()
{
   # Display Help
   echo "build and create docker image form smartgears distribution"
   echo
   echo "Syntax: buildDistribution [-g arg] [-j arg] [-p|u|h]"
   echo "options:"
   echo "-g arg      specifies the maven [g]oal {package, install, deploy etc} default is package."
   echo "-j arg      specify [j]ava version (default is $JAVA_VERSION)"
   echo "            accepted version are: ${ACCEPTED_JAVA_VERSIONS[@]}" 
   echo "-m          build docker image for [m]ultiple platform (must be suppported by local docker agent)"
   echo "-p          [p]ush image to d4science harbor (with login already done, or -l to login)"
   echo "-l          [l]ogin to d4science harbor"
   echo "-u          p[u]sh image to dockerhub (with docker login already done)"
   echo "-r          specify the [r]epository (default is $HARBOR_REPO)"
   echo "-h          Print this [h]elp."
   echo
   echo "to build a multiplatform image and push on d4science harbor"
   echo "    ./buildDistribution.sh -m -l -p"
}

################################################################################
################################################################################
# Main program                                                                 #
################################################################################
################################################################################



while getopts g:muplj:r:h flag 
do
   case "${flag}" in
      g) GOAL=${OPTARG};;
      m) MULTI_PLATFORM=true ;;
      u) PUSH_DOCKER=true ;;
      p) PUSH_HARBOR=true ;;
      l) LOGIN_HARBOR=true ;;
      j) if [[ ${ACCEPTED_JAVA_VERSIONS[@]} =~ ${OPTARG} ]] 
            then JAVA_VERSION=${OPTARG};
            else echo "Invalid java version" && echo "accepted version are: ${ACCEPTED_JAVA_VERSIONS[@]}" && exit 1;
         fi;;
      r) HARBOR_REPO=${OPTARG};;
      h) Help
         exit 0;;
      *) echo "Invalid option"
         exit 1;;
   esac
done

IMAGE_VERSION=${SMARTGEARS_VERSION}-java${JAVA_VERSION}-tomcat-${TOMCAT_VERSION}

BUILD_NAME=$NAME:$IMAGE_VERSION

echo "BUILD_NAME=$BUILD_NAME"

if [ ${LOGIN_HARBOR} = true ]; 
   then
   ./loginHarborHub.sh -r $HARBOR_REPO
fi

if [ -z $GOAL ];
   then	
      mvn clean package; 
   else 
      mvn clean ${GOAL}; 
fi

if [ $MULTI_PLATFORM ];
	then 
      echo "build multiplatform"
      docker build -t $BUILD_NAME --build-arg="JAVA_VERSION=${JAVA_VERSION}" --build-arg="SMARTGEARS_VERSION=${SMARTGEARS_VERSION}"  --platform=linux/amd64,linux/arm64,linux/arm/v7  .;
	else 
      echo "build single platform"
      docker build -t $BUILD_NAME --build-arg="JAVA_VERSION=${JAVA_VERSION}" --build-arg="SMARTGEARS_VERSION=${SMARTGEARS_VERSION}" . ;
fi

echo ">>> generated docker image ${IMAGE_VERSION}"


if [ ${PUSH_DOCKER} = true ]; 
   then 
      DOCKER_NAME=d4science/$BUILD_NAME
      docker tag $BUILD_NAME $DOCKER_NAME;
      docker push $DOCKER_NAME;
      echo ">>> pushed on dockerhub the image $DOCKER_NAME"
fi



if [ ${PUSH_HARBOR} = true ]; 
   then
      HARBOR_NAME=$HARBOR_REPO/gcube/$BUILD_NAME
      echo ">>> PUSHING on $HARBOR_REPO the image $HARBOR_NAME"
      docker tag $BUILD_NAME $HARBOR_NAME;
      docker push $HARBOR_NAME;
      echo ">>> pushed on $HARBOR_REPO the image $HARBOR_NAME"
fi

