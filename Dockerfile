ARG JAVA_VERSION=17

FROM tomcat:10.1.16-jdk${JAVA_VERSION}-temurin-jammy
  ARG SMARTGEARS_VERSION=""
  ENV FILENAME=smartgears-distribution-${SMARTGEARS_VERSION}

  COPY ./target/${FILENAME}.tar.gz /smartgears-distro.tar.gz 	
  WORKDIR /
  # all file and folder in etc/config are moved to the smartgears applications configuration directory   
  # for specific application configuration ( configuration file for app with contextPath "contextPath" must be placed in /etc/config/contextPath) 
  RUN mkdir -p /etc/config
  RUN tar zxvf /smartgears-distro.tar.gz
  RUN rm /smartgears-distro.tar.gz
  RUN export CATALINA_HOME=/usr/local/tomcat
  RUN ln -s /usr/local/tomcat tomcat
  RUN mv smartgears-distribution-$SMARTGEARS_VERSION  smartgears-distribution
  ENV GHN_HOME=./smartgears-distribution
  RUN ./smartgears-distribution/install -s tomcat   
  COPY tomcat_files/startContainer.sh /startContainer.sh
  RUN chmod +x /startContainer.sh
  ENTRYPOINT ["/startContainer.sh"]
  CMD ["catalina.sh","run"]