#!/bin/bash

logs=/etc/logback.xml
container=/etc/container.ini
configfolder=/etc/config

if [ -f "$logs" ]; then
 cp $logs /tomcat/lib
fi

if [ -f "$container" ]; then
 cp $container /smartgears-distribution
fi

if [ -d "$configfolder" ] && ! [ -z "$(ls -A $configfolder)" ]; then
 echo "config folder exists and is not empty"
 cp -R $configfolder/* /smartgears-distribution/config/apps/ 	
fi 

exec "$@"


