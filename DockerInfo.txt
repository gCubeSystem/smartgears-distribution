for current platform:
docker build -t smartgears-distribution:4.0.0-SNAPSHOT-java11-tomcat9 .

for multiple platforms:
docker build -t smartgears-distribution:4.0.0-SNAPSHOT-java11-tomcat9 --platform=linux/amd64,linux/arm64,linux/arm/v7 .